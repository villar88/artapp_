import React, { useState } from 'react';
import PropTypes from 'prop-types'

const Art = ({ imageUrl, extraData }) => {
  const [show, setShow] = useState(false);
  const handleModalClose = (e) => {
    const currentClass = e.target.className;
    if (currentClass === 'modal-card') {
      return;
    }
    setShow(false);
  };
  const handleModalOpen = () => {
    setShow(true);
  };
  return (
      <div className="App">
        <div hidden={!show} >
          <div className="modal-background" onClick={handleModalClose} >
            <div className="modal-card">
                <p><strong>CreditLine:</strong> {extraData.creditLine ? extraData.creditLine : '-'}</p>
                <p><strong>City:</strong> {extraData.city ? extraData.city : '-'}</p>
                <p><strong>Country:</strong> {extraData.country ? extraData.country : '-'}</p>
                <p><strong>Department:</strong> {extraData.department ? extraData.department : '-'}</p>
                <p><strong>Dimensions:</strong> {extraData.dimensions ? extraData.dimensions : '-'}</p>
            </div>
          </div>
        </div>
        <div className="single-art">
          <button className="button" onClick={handleModalOpen}><img src={imageUrl} alt={extraData}/></button>
        </div>
      </div>);
};

Art.propTypes = {
  imageUrl: PropTypes.string.isRequired,
    extraData: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
}
Art.defaultProps = {
  imageUrl: '',
  extraData: [],
}

export default Art
