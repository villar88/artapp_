import React from 'react'
import Art from './Art'
import './Modal.css'
import PropTypes from "prop-types";

class ArtContainer extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      arts: [],
      extraData: {},
      isFetch: true,
    }
  }

  async componentDidMount () {
    const min = 1;
    const max = 822568;
    let rand = parseInt(min + Math.random() * (max - min));
    const baseUrl = 'https://collectionapi.metmuseum.org/public/collection/v1/objects/'
    const response = await fetch(`${baseUrl}${rand}`)
    const responseJson = await response.json()
    this.setState({ arts: responseJson, isFetch: false})
  }
  render () {
    const { isFetch, arts } = this.state
    setTimeout('document.location.reload()',10000);
    return (
      <>
        { isFetch && 'Loading...' }
        { (!isFetch && !arts) && 'No art founded' }
        <section className="arts-container">
          { arts.message || arts.primaryImage === '' ? <div className="container"><div className="error-message">
            ART NOT FOUND
          </div></div> : <Art
              imageUrl={arts.primaryImage}
              name={arts.objectName}
              extraData={arts}
          />}
        </section>
      </>
    )
  };
}
ArtContainer.propTypes = {
  art: PropTypes.array.isRequired,
  extraData: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
}
ArtContainer.defaultProps = {
  art: [],
  extraData: {},
}
export default ArtContainer;
