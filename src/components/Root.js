import React from 'react'
import ArtContainer from './ArtContainer'

const Root = () => (
  <>
    <ArtContainer />
  </>
)

export default Root
