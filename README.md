# ArtApp

Test React

## Clone de repository

```
git clone https://gitlab.com/villar88/artapp.git
cd artapp
```

## Name
ArtApp.

## Description
This proyect is a test

## Installation
When cloning the repository, from the terminal we place the following command "npm install" so that the necessary components are downloaded for the operation of the project.
```
npm install
```
## Usage
To run the project we place the following command in the console "npm start". 
```
npm start
```
## Operation
The project consumes the api from the following address 'https://collectionapi.metmuseum.org/public/collection/v1/objects/{id}' where the {id} is a random number, if it finds an object that has a valid image, the image will be displayed, selecting it will show additional information about the work, otherwise an error message will be displayed. The application loads every 10 seconds to display a new image.

## License
For open source projects, say how it is licensed.

## Project status
This proyect is a test.

